# Raspberry Pi Docker-Swarm with Ansible

## Prerequisites
Set the following options in the Raspberry Pi Imager (or the /boot) mount when creating the Pis for the Swarm:
* Set Hostname: hostN.local (where N is the Id)
* Enable SSH -> Use password authentication
* Set username and password
  * Username: pi
  * Password: [Your choice]
* Set locale settings
  * Time zone: [Your timezone]
  * Keyboard layout: [Your layout]

All hosts must be available under the IPs specified in the inventory. To ensure this, setup a static IP by appending the following to */etc/dhcpcd.conf* on the hosts:

```
interface eth0
static ip_address=192.168.10.?/24
static domain_name_servers=192.168.10.1 8.8.8.8
```

`Attention`: It is assumed that the Pis are in a 192.168.10.0/24 network with 192.168.10.1 as the manager node. If your setup differs you may have to change some Playbooks. Furthermore it is assumed that there is a WiFi network for the Pis to connect to the internet (e.g. to download Docker images). The environment variable *WIFI_SSID* and *WIFI_PASSWORD* (on the Ansible host) will be used for this connection.

Packages:
* `ansible`: To execute the Playbooks for the Swarm management

## Manage the Docker Swarm with Ansible
Each playbook has a corresponding script in the *scripts* folder. These must be run in the following order from the root directory (e.g. *scripts/example.sh*).

1. `setup_public_key_authentication`: Copies the SSH public-key from the Ansible host to the remote hosts (will ask for the SSH password of the remote hosts)
2. `prepare_hosts`: Prepares all hosts by updating/installing packages among other things
3. `create_swarm`: Creates a Docker Swarm with n manager node and m worker nodes (according to the inventory), deploys a private docker registry and a Portainer instance
4. (`destroy_swarm`: Commands all workers to leave the swarm and destroys the swarm on the manager node)

## Setup LEDs/Buttons/Displays

This repository includes a Systemd service to interact with a LED/Button/OLED-Display connected to the Raspberry Pi.

The hardware must be connected to the following pins:
* `Button`: 20
* `LED`: 23
* `Display`: 5 (SCL), 3 (SDA)

If you want to put the hardware neatly in a rack, you can use [these](https://www.thingiverse.com/thing:3022136) instructions on Thingiverse.

To install the service on all hosts, run the following:
```shell
scripts/install_io_service.sh
```

This will install and enable the Systemd service and create a ramfs mount (to prevent writing to the fragile SD cards) at */mnt/ram*. In this folder there will be 3 files:
* `button (Read-Only)`: Will contain 1 if the button is pressed, 0 otherwise
* `led`: Write 1 to this file to turn on the led or anything else to turn it off
* `display_flash`: Write 1 to this file to turn on all pixels on the display or anything else to turn them off

## Interact with the Swarm
The first option is to connect any of the manager hosts via SSH and use docker directly but is recommended to setup your local Docker client to use the remote daemon. To accomplish this set the environment variable *DOCKER_HOST* to *ssh://pi@192.168.10.1*:
```shell
export DOCKER_HOST="ssh://pi@192.168.10.1"
```

Whenever you now run a Docker command it will use the remote daemon. To verify this, run:
```shell
docker node ls
```

This should display the same amount of nodes as specified in the Ansible inventory.

The main adavantage is that it will also execute build commands on the remote daemon which ensures that they are build for the correct CPU architecture of the Pis.

## Some Commands To Try
Lists all nodes (hosts) in the Swarm cluster:
```shell
docker node ls
```

List all stacks:
```shell
docker stack ls
```

Deploy a stack:
```shell
docker stack deploy -c FILENAME STACK_NAME
```

Destroy a stack:
```shell
docker stack rm STACK_NAME
```

List all services:
```shell
docker service ls
```

Scale the replicas of a service:
```shell
docker service scale SERVICE_NAME=NEW_AMOUNT_OF_REPLICAS
```

Show more information about the status of a services (e.g. to see why it won't start):
```shell
docker service ps SERVICE_NAME --no-trunc
```
The flag *--no-trunc* prevents the status from being truncated.


Show the log output of a service:
```shell
docker service logs SERVICE_NAME
```
Use the flag *-f* to see the output in realtime.


Set state of a node:
```shell
docker node update --availability active|drain [Node Name]
```
Simulate a node "failure" with *drain* which will remove all containers from this nodes or reactivate it with *active*.

## Some Deployments To Try
This repository includes some example deployments to experiment with the Swarm (located under */stacks*).

### whoami
To deploy run *scripts/deploy_whoami.sh*. To cleanup run *scripts/destroy_whoami.sh*.

This deploys the *traefik/whoami* image which includes a webapp that serves information about the host (e.g. the hostname) on port 8080 (you can use the ip of any of the nodes inside the Swarm). An example usage would be to scale the replicas up/down and see how the loadbalancer balances requests between the hosts.

### blinking
To deploy run *scripts/deploy_blinking.sh*. To cleanup run *scripts/destroy_blinking.sh*.

This deployment contains an application that turns on/off the display at a fixed interval. An example usage would be to try how the Swarm handles disappearing nodes (e.g. by pulling the ethernet plug).

### blinking_webapp
To deploy run *scripts/deploy_blinking_webapp.sh*. To cleanup run *scripts/destroy_blinking_webapp.sh*.

This deployment serves a webapp on port 80 (you can use the ip of any of the nodes inside the Swarm) that turns on the display of the hosting node for a short period of time and returns the hostname. An example usage would be same as for *whoami* but in a more visual way.

### blinking_button
To deploy run *scripts/deploy_blinking_button.sh*. To cleanup run *scripts/destroy_blinking_button.sh*.

This deployment consists of two applications: Input and Output. The input app reads the local button on the Raspberry Pi and makes a POST request to the output application everytime it is pressed down. This application then turns the led on the local Raspberry Pi on for a short period. An example usage would be to experiment with internal loadbalancing and internal DNS resolution.

### blinking_button
To deploy run *scripts/deploy_container_communication.sh*. To cleanup run *scripts/destroy_container_communication.sh*.

This deployment consists of two web servers: Internal and External. The external server exposes a HTTP endpoint on port 80. Everytime it is called, it calls the internal application (on port 5000) which returns its hostname. The external server than attaches its own hostname and returns it to the original client. An example usage would be to experiment with internal name resolution of services (DNS) or internal load balancing.

## Helper Scripts
The folder *scripts/* also contains some helper scripts. Some of these are:
* `turn_off_leds.sh`: Turn of the LED on all nodes
* `turn_off_displays.sh`: Turn of the display on all nodes
* `turn_off_all.sh`: Turn of the LED and display on all nodes
* `nmap_host_scan.sh`: Scans the network *192.168.10.0/24* with nmap for hosts and lists there IPs among other information
* `reboot_hosts.sh`: Reboots all nodes
* `shutdown_hosts.sh`: Shuts all nodes down