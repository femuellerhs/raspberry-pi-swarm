#!/bin/bash
set -e

ansible-playbook playbooks/prepare_hosts.yml --extra-vars="WIFI_SSID='$WIFI_SSID' WIFI_PASSWORD='$WIFI_PASSWORD'"
