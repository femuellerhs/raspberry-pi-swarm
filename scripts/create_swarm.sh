#!/bin/bash
set -e

echo -e "\033[1;32m[Creating Swarm]\033[0m"
ansible-playbook playbooks/create_swarm.yml
echo -e "\033[1;32m[Deploying Docker Registry]\033[0m"
docker stack deploy -c stacks/registry.yml registry
echo -e "\033[1;32m[Deploying Portainer]\033[0m"
docker stack deploy -c stacks/portainer.yml portainer