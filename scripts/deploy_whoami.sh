#!/bin/bash

set -e

echo -e "\033[1;34m> docker stack deploy -c stacks/whoami.yml whoami\033[0m"
docker stack deploy -c stacks/whoami.yml whoami