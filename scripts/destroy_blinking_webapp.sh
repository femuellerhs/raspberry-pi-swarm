#!/bin/bash
set -e

echo -e "\033[1;34m> docker stack rm blinking_webapp\033[0m"
docker stack rm blinking_webapp
