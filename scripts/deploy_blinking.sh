#!/bin/bash

set -e

echo -e "\033[1;32m[Building Docker Image]\033[0m"
echo -e "\033[1;34m> docker build -t 192.168.10.1:5000/blinking:latest stacks/blinking\033[0m"
docker build -t 192.168.10.1:5000/blinking:latest stacks/blinking
echo -e "\033[1;32m[Pushing Docker Image To Registry]\033[0m"
echo -e "\033[1;34m> docker push 192.168.10.1:5000/blinking\033[0m"
docker push 192.168.10.1:5000/blinking
echo -e "\033[1;32m[Deploying On Swarm]\033[0m"
echo -e "\033[1;34m> docker stack deploy -c stacks/blinking/deployment.yml blinking\033[0m"
docker stack deploy -c stacks/blinking/deployment.yml blinking