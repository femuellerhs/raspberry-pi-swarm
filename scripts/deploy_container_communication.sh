#!/bin/bash

set -e

echo -e "\033[1;32m[Building Internal Docker Image]\033[0m"
echo -e "\033[1;34m> docker build -t 192.168.10.1:5000/container_communication_internal:latest stacks/container_communication/internal\033[0m"
docker build -t 192.168.10.1:5000/container_communication_internal:latest stacks/container_communication/internal
echo -e "\033[1;32m[Building External Docker Image]\033[0m"
echo -e "\033[1;34m> docker build -t 192.168.10.1:5000/container_communication_external:latest stacks/container_communication/external\033[0m"
docker build -t 192.168.10.1:5000/container_communication_external:latest stacks/container_communication/external
echo -e "\033[1;32m[Pushing Internal Docker Image To Registry]\033[0m"
echo -e "\033[1;34m> docker push 192.168.10.1:5000/container_communication_internal\033[0m"
docker push 192.168.10.1:5000/container_communication_internal
echo -e "\033[1;32m[Pushing External Docker Image To Registry]\033[0m"
echo -e "\033[1;34m> docker push 192.168.10.1:5000/container_communication_external\033[0m"
docker push 192.168.10.1:5000/container_communication_external
echo -e "\033[1;32m[Deploying On Swarm]\033[0m"
echo -e "\033[1;34m> docker stack deploy -c stacks/container_communication/deployment.yml blinking_button\033[0m"
docker stack deploy -c stacks/container_communication/deployment.yml container_communication