#!/bin/bash

set -e

echo -e "\033[1;32m[Building Input Docker Image]\033[0m"
echo -e "\033[1;34m> docker build -t 192.168.10.1:5000/blinking_button_input:latest stacks/blinking_button/input\033[0m"
docker build -t 192.168.10.1:5000/blinking_button_input:latest stacks/blinking_button/input
echo -e "\033[1;32m[Building Output Docker Image]\033[0m"
echo -e "\033[1;34m> docker build -t 192.168.10.1:5000/blinking_button_output:latest stacks/blinking_button/output\033[0m"
docker build -t 192.168.10.1:5000/blinking_button_output:latest stacks/blinking_button/output
echo -e "\033[1;32m[Pushing Input Docker Image To Registry]\033[0m"
echo -e "\033[1;34m> docker push 192.168.10.1:5000/blinking_button_input\033[0m"
docker push 192.168.10.1:5000/blinking_button_input
echo -e "\033[1;32m[Pushing Output Docker Image To Registry]\033[0m"
echo -e "\033[1;34m> docker push 192.168.10.1:5000/blinking_button_output\033[0m"
docker push 192.168.10.1:5000/blinking_button_output
echo -e "\033[1;32m[Deploying On Swarm]\033[0m"
echo -e "\033[1;34m> docker stack deploy -c stacks/blinking_button/deployment.yml blinking_button\033[0m"
docker stack deploy -c stacks/blinking_button/deployment.yml blinking_button