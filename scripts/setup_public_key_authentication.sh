#!/bin/bash
set -e

ansible-playbook playbooks/setup_public_key_authentication.yml --extra-vars="ssh_public_key='$(cat ~/.ssh/id_rsa.pub)'" --ask-pass
