import time

def set_state(state: bool):
    with open("/mnt/io/led", "w") as f:
        f.write("1" if state else "0")
    with open("/mnt/io/display_flash", "w") as f:
        f.write("1" if state else "0")


def main():
    last_state = False
    while True:
        t = time.time()
        state = t - int(t) < 0.5
        if state != last_state:
            set_state(state)
            last_state = state
        time.sleep(0.001)


if __name__ == '__main__':
    try:
        main()
    except Exception:
        set_state(False)
