import requests


def main():
    last_state = False
    counter = 0
    while True:
        with open("/mnt/io/button", "r") as f:
            state = f.readline().strip() == "1"

        if not last_state and state:
            counter += 1
            print(f"Button pressed {counter} times")
            try:
                requests.post("http://output:5000", timeout=3)
            except Exception:
                pass
        
        last_state = state


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        print(e)
