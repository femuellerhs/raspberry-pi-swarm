import socket
from flask import Flask

app = Flask(__name__)


@app.after_request
def add_header(r):
    """
    Add headers to prevent browsers from caching
    
    Source: https://stackoverflow.com/questions/34066804/disabling-caching-in-flask
    """
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers['Cache-Control'] = 'public, max-age=0'
    return r


@app.route('/', methods=['POST'])
def index():  
    return "Internal: " + socket.gethostname()


if __name__ == '__main__':
    try:
        app.run(host='0.0.0.0')
    except Exception:
        pass
