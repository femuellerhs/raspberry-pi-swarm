import time
import os
import busio
import RPi.GPIO as GPIO
import adafruit_ssd1306
from board import SCL, SDA
from PIL import Image, ImageDraw, ImageFont

BUTTON = 20
LED = 23

GPIO.setmode(GPIO.BCM)
GPIO.setup(BUTTON, GPIO.IN)
GPIO.setup(LED, GPIO.OUT)

i2c = busio.I2C(SCL, SDA)
display = adafruit_ssd1306.SSD1306_I2C(128, 32, i2c)
display.rotation = 2

last_button_pressed = False
if not os.path.exists("/mnt/ram/button"):
    with open("/mnt/ram/button", "w") as f:
        f.write("0")
else:
    with open("/mnt/ram/button", "r") as f:
        last_button_pressed = f.readline().strip() == "1"
if not os.path.exists("/mnt/ram/led"):
    os.mknod("/mnt/ram/led")
last_display_flash = False
if not os.path.exists("/mnt/ram/display_flash"):
    with open("/mnt/ram/display_flash", "w") as f:
        f.write("0")
else:
    with open("/mnt/ram/button", "r") as f:
        last_display_flash = f.readline().strip() == "1"
if last_display_flash:
    display.fill(1)
else:
    display.fill(0)
display.show()

def button_task():
    global last_button_pressed

    state = GPIO.input(BUTTON) == 0
    if state != last_button_pressed:
        with open("/mnt/ram/button", "w") as f:
            f.write("1" if state else "0")
        last_button_pressed = state


def led_task():
    with open("/mnt/ram/led", "r") as f:
        state = f.readline().strip() == "1"

    if state:
        GPIO.output(LED, GPIO.HIGH)
    else:
        GPIO.output(LED, GPIO.LOW)


def display_flash_task():
    global last_display_flash

    with open("/mnt/ram/display_flash", "r") as f:
        state = f.readline().strip() == "1"
    
    if state == last_display_flash:
        return

    if state:
        display.fill(1)
    else:
        display.fill(0)
    display.show()
    last_display_flash = state


def main():
    while True:
        try:
            button_task()
            led_task()
            display_flash_task()
        except Exception as e:
            print("Failed:", e)
        time.sleep(0.001)


if __name__ == '__main__':
    main()
